from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Question(models.Model):
    title = models.TextField()
    status = models.CharField(default='Invalid', max_length=10)
    createdBy = models.ForeignKey(User, on_delete="CASCADE")
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class Choice(models.Model):
    quetion = models.ForeignKey('poll.Question', on_delete="CASCADE")
    text = models.TextField()
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.text
